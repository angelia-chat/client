
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: 'new-meeting', component: () => import('pages/NewMeeting.vue') }
    ]
  },

  {
    path: '/meeting/:code',
    component: () => import('layouts/WholeScreen.vue'),
    children: [
      { path: '', component: () => import('pages/Meeting.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
