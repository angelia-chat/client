#!/bin/sh
set -e
. /code/docker/scripts/base.sh

exec /sbin/su-exec user quasar dev --port $APP_HOST_PORT