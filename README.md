# Angelia Chat

Simple P2P chat / video conferencing, mostly client side. You'll need to add a TURN server in the config to connect behind symmetric NATs.


## Running

The only requirement is a recent Docker version.

Copy `.env.template` to `.env` and adjust as necessary.

Run:

    $ docker compose up


### Live version

https://chat.us.davidrios.dev/#/
