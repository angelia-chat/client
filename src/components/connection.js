import Peer from 'peerjs'
import { LocalStorage } from 'quasar'

import { sleep } from './utils'

export class Connection extends EventTarget {
  constructor (roomCode, isHost) {
    super()

    this._roomCode = roomCode
    this._isHost = isHost
    this._conn = null

    this._state = null
    this._stateIteration = 0

    this._lastMessageId = 0
    this._sentMessages = {}
    this._receivedResponses = {}

    this._peerConnection = null
    this._localStreams = {}
    this._remoteStreams = {}
    this._remoteStreamsMeta = {}
    this._sentTracks = {}
    this._ignoreOffer = false
  }

  get state () {
    return this._state
  }

  set state (newState) {
    this._state = newState
    this._stateIteration += 1
    console.log('new state', newState)
  }

  async _setStateAfter (state, ms, query) {
    const lastIteration = this._stateIteration

    if (query == null || query > ms || query <= 0) {
      query = ms
    }

    let waited = 0
    while (true) {
      await sleep(query)

      waited += query
      if (waited >= ms) {
        break
      }

      if (this._stateIteration !== lastIteration) {
        console.log('state changed while querying, skipping')
        return
      }
    }

    if (this._stateIteration === lastIteration) {
      this.state = state
      return true
    }
  }

  async _handleCallDescription (description) {
    this._setupPeerConnection()

    console.warn('handle before')

    const offerCollision = (
      description.type === 'offer' &&
      (
        this.state === 'call_in_progress' ||
        this._peerConnection.signalingState !== 'stable'
      )
    )

    this._ignoreOffer = !this._isHost && offerCollision
    if (this._ignoreOffer) {
      return
    }

    console.warn('handle after ignore')

    await this._peerConnection.setRemoteDescription(description)
    console.warn('2')
    if (description.type === 'offer') {
      console.warn('3')
      await this._peerConnection.setLocalDescription()
      this._conn.send({
        message: 'call_description',
        description: this._peerConnection.localDescription.toJSON()
      })
    }

    this.state = 'call_in_progress'
  }

  async _handleIceCandidate (candidate) {
    this._setupPeerConnection()

    try {
      await this._peerConnection.addIceCandidate(candidate)
    } catch (err) {
      if (!this._ignoreOffer) {
        console.log('error setting ice candidate', err)
      }
    }
  }

  _handleData (data) {
    console.log('received data', data)

    switch (data.message) {
      case 'guest_connect': // host
        this._conn.send({
          message: 'response',
          messageId: data.messageId,
          accepted: true
        })
        this.state = 'established'
        {
          const ev = new Event('guest_connected')
          ev.peerId = this._conn.peer
          this.dispatchEvent(ev)
        }
        break

      case 'response': // host/guest
        this._receivedResponses[data.messageId] = data
        break

      case 'prepare_for_call': // host/guest
        this._setupPeerConnection()
        this._conn.send({
          message: 'response',
          messageId: data.messageId
        })
        break

      case 'call_stream_meta': // host/guest
        this._remoteStreamsMeta[data.camera] = 'camera'
        this._remoteStreamsMeta[data.screen] = 'screen'
        this._conn.send({
          message: 'response',
          messageId: data.messageId
        })
        break

      case 'call_description': // host/guest
        this._handleCallDescription(data.description)
        break

      case 'ice_candidate': // host/guest
        this._handleIceCandidate(data.candidate)
        break
    }
  }

  async _sendAndWait (message, retries = 3, waitFor = 500) {
    this._lastMessageId += 1
    const messageId = this._lastMessageId

    this._sentMessages[messageId] = message

    console.log('waiting response', messageId, message)

    let res = null
    for (let i = 0; i < retries; i++) {
      res = await this._waitForResponse(messageId, waitFor)
      if (res != null) {
        break
      }
      console.log('timedout', messageId, message)
    }

    delete this._sentMessages[messageId]
    if (res == null) {
      throw new Error('failed')
    }

    return res
  }

  async _waitForResponse (messageId, waitFor = 500) {
    this._conn.send({
      ...this._sentMessages[messageId],
      messageId: messageId
    })

    const iterations = Math.ceil(waitFor / 100)
    for (let i = 0; i < iterations; i++) {
      if (this._receivedResponses[messageId] != null) {
        break
      }
      await sleep(100)
    }

    const res = this._receivedResponses[messageId]
    delete this._receivedResponses[messageId]
    return res
  }

  _setup () {
    if (this._isHost) {
      this.peer.on('connection', async (conn) => {
        console.log('received connection', conn)

        if (this.state !== 'waiting_guest') {
          console.log('rejecting unwanted new connection')
          return
        }

        this._conn = conn

        const ev = new Event('peer_connected')
        ev.peerId = conn.peer
        this.dispatchEvent(ev)

        conn.on('close', () => {
          if (conn.peer !== this._conn.peer) {
            console.log('ignoring event from wrong peer')
            return
          }

          this.state = 'waiting_guest'
          this._clearPeerConnection()

          const ev = new Event('peer_disconnected')
          ev.peerId = conn.peer
          this.dispatchEvent(ev)
        })

        conn.on('error', (err) => {
          if (conn.peer !== this._conn.peer) {
            console.log('ignoring event from wrong peer')
            return
          }

          this.state = 'waiting_guest'
          this._clearPeerConnection()

          const ev = new Event('peer_error')
          ev.peerId = conn.peer
          ev.error = err
          this.dispatchEvent(ev)
        })

        conn.on('data', (data) => {
          if (conn.peer !== this._conn.peer) {
            console.log('ignoring message from wrong peer')
            return
          }

          this._handleData(data)
        })
      })
    } else {
      (async function (this_) {
        for (let i = 0; i < 3; i++) {
          if (this_.state !== 'ready') {
            break
          }

          this_.currentTry = i

          if (this_._conn != null) {
            this_._conn.close()
            await sleep(100)
          }

          const conn = this_.peer.connect(this_._roomCode, { reliable: true })
          this_._conn = conn

          conn.on('open', async () => {
            console.log('open')
            if (i !== this_.currentTry) {
              console.log('wrong try')
              return
            }

            console.log('connection open')
            this_.state = 'connection_open'

            this_.dispatchEvent(new Event('validating_connection'))

            let response = null
            try {
              response = await this_._sendAndWait({ message: 'guest_connect' })
            } catch (err) {
              console.log(err)
              this_.state = 'finished'
              this_.dispatchEvent(new Event('connection_error'))
              return
            }

            if (response.accepted) {
              this_.state = 'established'
              const ev = new Event('connection_accepted')
              ev.peerId = this_._conn.peer
              this_.dispatchEvent(ev)
            } else {
              this_.state = 'finished'
              const ev = new Event('connection_rejected')
              ev.peerId = this_._conn.peer
              this_.dispatchEvent(ev)
            }
          })

          conn.on('close', () => {
            if (i !== this_.currentTry) {
              console.log('wrong try')
              return
            }

            this_.state = 'finished'
            this_._clearPeerConnection()

            this_.dispatchEvent(new Event('connection_closed'))
          })

          conn.on('error', (err) => {
            if (i !== this_.currentTry) {
              console.log('wrong try')
              return
            }

            this_.state = 'finished'
            this_._clearPeerConnection()

            const ev = new Event('connection_error')
            ev.error = err
            this_.dispatchEvent(ev)
          })

          conn.on('data', (data) => {
            if (i !== this_.currentTry) {
              console.log('wrong try')
              return
            }

            this_._handleData(data)
          })

          await sleep(2000)
        }

        if (this_.state === 'ready') {
          this_.state = 'finished'
          this_.dispatchEvent(new Event('connection_error'))
        }
      })(this)
    }
  }

  async connect () {
    this.state = 'started'

    const iceServers = []

    for (const item of LocalStorage.getItem('stunServers') || []) {
      iceServers.push({ urls: 'stun:' + item.url })
    }

    if (iceServers.length === 0) {
      iceServers.push({ urls: 'stun:stun.l.google.com:19302' })
    }

    for (const item of LocalStorage.getItem('turnServers') || []) {
      iceServers.push({ urls: 'turn:' + item.url, username: item.username, credential: item.credential })
    }

    this.peer = new Peer(
      this._isHost ? this._roomCode : null,
      {
        config: {
          iceServers: iceServers,
          sdpSemantics: 'unified-plan'
        }
      }
    )

    for (let i = 0; i < 20; i++) {
      if (this.state === 'started' && !this.peer.disconnected) {
        await sleep(100)
        this.state = 'waiting'
        continue
      }

      if (this.peer.disconnected || this.peer.id == null) {
        console.log('wait connection')
        await sleep(100)
        continue
      }

      this.state = this._isHost ? 'waiting_guest' : 'ready'

      this._setup()

      return
    }

    this.state = 'error'
    throw new Error('connection failed')
  }

  close () {
    this.peer.destroy()
  }

  _clearPeerConnection () {
    this._peerConnection?.close()
    this._peerConnection = null

    for (const stream of Object.values(this._localStreams)) {
      for (const track of stream.getTracks()) {
        stream.removeTrack(track)
      }
    }

    this._localStreams = {}
    this._sentTracks = {}
  }

  _setupPeerConnection () {
    if (this._peerConnection != null) {
      return
    }

    this._peerConnection = new RTCPeerConnection({
      iceServers: this.peer.options.config.iceServers
    })

    this._peerConnection.onnegotiationneeded = async () => {
      try {
        await this._peerConnection.setLocalDescription()
        this._conn.send({
          message: 'call_description',
          description: this._peerConnection.localDescription.toJSON()
        })
      } catch (err) {
        console.error(err)
      }
    }

    this._peerConnection.onicecandidate = ({ candidate }) => {
      if (candidate == null) {
        return
      }

      this._conn.send({
        message: 'ice_candidate',
        candidate: candidate.toJSON()
      })
    }

    this._localStreams = {
      camera: new MediaStream(),
      screen: new MediaStream()
    }

    const handleRemoveTrack = () => {
      const ev = new Event('call_stream_update')
      ev.remoteStreams = this._remoteStreams
      ev.peerId = this._conn.peer
      this.dispatchEvent(ev)
    }

    this._peerConnection.ontrack = ({ track, streams }) => {
      console.log('ontrack', track, streams)

      const remoteStream = streams[0]
      remoteStream.onremovetrack = handleRemoveTrack

      remoteStream.addTrack(track)
      this._remoteStreams[this._remoteStreamsMeta[remoteStream.id]] = remoteStream

      const ev = new Event('call_stream_update')
      ev.remoteStreams = this._remoteStreams
      ev.peerId = this._conn.peer
      this.dispatchEvent(ev)
    }
  }

  async updateCall (tracks) {
    if (this.state !== 'established' && this.state !== 'call_in_progress') {
      throw new Error('call_not_ready')
    }

    if (this.state === 'established' && !(tracks.camera || tracks.mic || tracks.screen)) {
      console.log('no tracks to send, not calling')
      return
    }

    console.log('updating call')

    // this.state = 'trying_call'

    await this._sendAndWait({ message: 'prepare_for_call' })

    this._setupPeerConnection()

    await this._sendAndWait({
      message: 'call_stream_meta',
      camera: this._localStreams.camera.id,
      screen: this._localStreams.screen.id
    })

    for (const trackType of ['camera', 'mic', 'screen']) {
      const track = tracks[trackType]

      if (this._sentTracks[trackType] != null) {
        if (track == null || this._sentTracks[trackType].track.id !== track.id) {
          this._peerConnection.removeTrack(this._sentTracks[trackType].sender)
          delete this._sentTracks[trackType]
        }
      }

      if (track == null || this._sentTracks[trackType]?.track.id === track.id) {
        continue
      }

      const sender = this._peerConnection.addTrack(
        track, this._localStreams[{
          camera: 'camera',
          mic: 'camera',
          screen: 'screen'
        }[trackType]]
      )
      this._sentTracks[trackType] = { track, sender }
    }

    // const changed = await this._setStateAfter('call_hanged', 3000, 100)
    // if (changed) {
    //   if (this._isHost) {
    //     this._sendAndWait({ message: 'call_hanged' })
    //   } else {
    //     this.dispatchEvent(new Event('call_hanged'))
    //   }
    // }
  }
}
